/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.Serializable;

/**
 *
 * @author informatics
 */
public class friend implements Serializable{

    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastId = 1;

    public friend(String name, int age, String tel) {
        this.id = lastId++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if (age < 0) {
            throw new Exception();
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }

    public static void main(String[] args) {
        friend f1 = new friend("Win", 10, "0818555555");
        friend f2 = new friend("Benz", 22, "0818555555");
        try {
            f1.setAge(-1);
            System.out.println(f1.toString());
            System.out.println(f2.toString());
        } catch (Exception ex) {
            System.out.println("Error!!!");
        }

    }
}
