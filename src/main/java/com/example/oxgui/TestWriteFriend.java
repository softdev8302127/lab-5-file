/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 *
 * @author informatics
 */
public class TestWriteFriend {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        friend f1 = new friend("Win", 10, "0818555555");
        friend f2 = new friend("Benz", 22, "0818555555");
        System.out.println(f1.toString());
        System.out.println(f2.toString());
        File file = new File("friend.dat");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(f1);
        oos.writeObject(f2);
        oos.close();
        fos.close();
    }
}
