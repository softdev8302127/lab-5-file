/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author informatics
 */
public class TestReadFriend {

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream fis = null;
        try {
            File file = new File("friend.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            friend f1 = (friend) ois.readObject();
            friend f2 = (friend) ois.readObject();
            System.out.println(f1);
            System.out.println(f1);

            ois.close();
            fis.close();
        } catch (Exception ex) {
            System.out.println("Error");
        }finally{
            try {
                if(fis!=null){
                   fis.close(); 
                } 
            } catch (Exception ex) {
            }
        }

    }
}
